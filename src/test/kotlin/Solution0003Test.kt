import me.djsushi.kotlinleet.Solution0003
import kotlin.test.Test
import kotlin.test.assertEquals

class Solution0003Test {

    @Test
    fun `Example 1`() {
        val s = "abcabcbb"
        val length = Solution0003.lengthOfLongestSubstring(s)
        assertEquals(3, length)
    }

    @Test
    fun `Example 2`() {
        val s = "bbbbb"
        val length = Solution0003.lengthOfLongestSubstring(s)
        assertEquals(1, length)
    }

    @Test
    fun `Example 3`() {
        val s = "pwwkew"
        val length = Solution0003.lengthOfLongestSubstring(s)
        assertEquals(3, length)
    }

    @Test
    fun `Short input`() {
        assertEquals(0, Solution0003.lengthOfLongestSubstring(""))
        assertEquals(1, Solution0003.lengthOfLongestSubstring("a"))
        assertEquals(1, Solution0003.lengthOfLongestSubstring("1"))
        assertEquals(1, Solution0003.lengthOfLongestSubstring("aa"))
        assertEquals(2, Solution0003.lengthOfLongestSubstring("ab"))
    }
}