import me.djsushi.kotlinleet.ListNode
import me.djsushi.kotlinleet.Solution0001
import me.djsushi.kotlinleet.Solution0002
import kotlin.test.*

class Solution0002Test {

    @Test
    fun `Example 1`() {
        val l11 = ListNode(2)
        val l12 = ListNode(4)
        val l13 = ListNode(3)
        l11.next = l12
        l12.next = l13

        val l21 = ListNode(5)
        val l22 = ListNode(6)
        val l23 = ListNode(4)
        l21.next = l22
        l22.next = l23

        val sum = Solution0002.addTwoNumbers(l11, l21)
        assertNotNull(sum)
        assertNull(sum.next?.next?.next)
        assertEquals(7, sum.`val`)
        assertEquals(0, sum.next?.`val`)
        assertEquals(8, sum.next?.next?.`val`)
    }

    @Test
    fun `Example 2`() {
        val l11 = ListNode(0)
        val l21 = ListNode(0)

        val sum = Solution0002.addTwoNumbers(l11, l21)
        assertNotNull(sum)
        assertNull(sum.next)
        assertEquals(0, sum.`val`)
    }

    @Test
    fun `Example 3`() {
        val l11 = ListNode(9)
        val l12 = ListNode(9)
        val l13 = ListNode(9)
        val l14 = ListNode(9)
        val l15 = ListNode(9)
        val l16 = ListNode(9)
        val l17 = ListNode(9)
        l11.next = l12
        l12.next = l13
        l13.next = l14
        l14.next = l15
        l15.next = l16
        l16.next = l17

        val l21 = ListNode(9)
        val l22 = ListNode(9)
        val l23 = ListNode(9)
        val l24 = ListNode(9)
        l21.next = l22
        l22.next = l23
        l23.next = l24

        val sum = Solution0002.addTwoNumbers(l11, l21)
        assertNotNull(sum?.next?.next?.next?.next?.next?.next?.next?.`val`)
        assertEquals(8, sum?.`val`)
        assertEquals(9, sum?.next?.`val`)
        assertEquals(9, sum?.next?.next?.`val`)
        assertEquals(9, sum?.next?.next?.next?.`val`)
        assertEquals(0, sum?.next?.next?.next?.next?.`val`)
        assertEquals(0, sum?.next?.next?.next?.next?.next?.`val`)
        assertEquals(0, sum?.next?.next?.next?.next?.next?.next?.`val`)
        assertEquals(1, sum?.next?.next?.next?.next?.next?.next?.next?.`val`)
    }
}