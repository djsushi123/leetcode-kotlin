import me.djsushi.kotlinleet.Solution0001
import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

class Solution0001Test {

    @Test
    fun `Example 1`() {
        val numbers = intArrayOf(2, 7, 11, 15)
        val target = 9
        val result = Solution0001.twoSum(numbers, target)
        assertEquals(2, result.size)
        assertContains(result, 0)
        assertContains(result, 1)
    }

    @Test
    fun `Example 2`() {
        val numbers = intArrayOf(3, 2, 4)
        val target = 6
        val result = Solution0001.twoSum(numbers, target)
        assertEquals(2, result.size)
        assertContains(result, 1)
        assertContains(result, 2)
    }

    @Test
    fun `Example 3`() {
        val numbers = intArrayOf(3, 3)
        val target = 6
        val result = Solution0001.twoSum(numbers, target)
        assertEquals(2, result.size)
        assertContains(result, 0)
        assertContains(result, 1)
    }
}