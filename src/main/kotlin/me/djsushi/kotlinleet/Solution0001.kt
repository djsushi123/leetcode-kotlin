package me.djsushi.kotlinleet

import me.djsushi.kotlinleet.processor.api.Solution

@Solution("twoSum")
object Solution0001 {

    fun twoSum(nums: IntArray, target: Int): IntArray {
        for (index1 in 0..nums.lastIndex) {
            for (index2 in (index1 + 1)..nums.lastIndex) {
                if (nums[index1] + nums[index2] == target) return intArrayOf(index1, index2)
            }
        }
        // Impossible scenario
        throw NoSuchElementException("No two numbers added up to the target of $target.")
    }
}
