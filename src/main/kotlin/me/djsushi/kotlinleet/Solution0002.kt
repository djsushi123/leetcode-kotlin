package me.djsushi.kotlinleet

import me.djsushi.kotlinleet.processor.api.Solution
import java.math.BigInteger

/** Yep, this [val] thing is for real and we can't change it :D. */
class ListNode(var `val`: Int) {

    var next: ListNode? = null
}

@Solution("addTwoNumbers")
object Solution0002 {

    private fun readNumber(listNode: ListNode) = buildString {
        var currentNode: ListNode? = listNode
        while (currentNode != null) {
            append(currentNode.`val`)
            currentNode = currentNode.next
        }
    }.reversed().toBigInteger()

    private fun BigInteger.toListNodeNumber(): ListNode = toString().reversed().run {
        val returnNode = ListNode(first().digitToInt())
        var currentNode: ListNode? = returnNode
        drop(1).forEach { digit ->
            currentNode?.next = ListNode(digit.digitToInt())
            currentNode = currentNode?.next
        }
        returnNode
    }

    @Suppress("RedundantNullableReturnType")
    fun addTwoNumbers(l1: ListNode?, l2: ListNode?): ListNode? {
        val num1 = readNumber(l1!!)
        val num2 = readNumber(l2!!)
        return num1.plus(num2).toListNodeNumber()
    }
}
