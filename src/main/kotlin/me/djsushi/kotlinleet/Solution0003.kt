package me.djsushi.kotlinleet

import me.djsushi.kotlinleet.processor.api.Solution
import kotlin.math.max

@Solution("lengthOfLongestSubstring")
object Solution0003 {

    fun lengthOfLongestSubstring(s: String): Int {
        var longestSubstringLength = 0
        val visited = mutableSetOf<Char>()
        var rightPointer = 0
        var leftPointer = 0
        while (rightPointer < s.length) {
            while (visited.contains(s[rightPointer])) {
                visited.remove(s[leftPointer])
                leftPointer++
            }
            visited.add(s[rightPointer])
            longestSubstringLength = max(longestSubstringLength, rightPointer - leftPointer + 1)
            rightPointer++
        }
        return longestSubstringLength
    }
}
