plugins {
    `java-library`
    kotlin("jvm") version "1.9.10"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

//buildscript {
//    dependencies {
//        classpath(kotlin("gradle-plugin", version = "1.9.10"))
//    }
//}

dependencies {
    implementation(project(":processor-api"))
    runtimeOnly("com.squareup:kotlinpoet:1.14.2")
    runtimeOnly("com.squareup:kotlinpoet-ksp:1.14.2")
    implementation("com.google.devtools.ksp:symbol-processing-api:1.9.10-1.0.13")
}

tasks.test {
    useJUnitPlatform()
}