package me.djsushi.kotlinleet.processor

import com.google.devtools.ksp.processing.*
import com.google.devtools.ksp.symbol.KSAnnotated
import com.google.devtools.ksp.symbol.KSClassDeclaration

class Processor(private val logger: KSPLogger) : SymbolProcessor {

    override fun process(resolver: Resolver): List<KSAnnotated> {
        val problemClasses = resolver.getSymbolsWithAnnotation("me.djsushi.kotlinleet.processor.api.Solution")

        problemClasses.forEach { clazz ->
            clazz as KSClassDeclaration
            val problemNumber = extractNumberFromClassName(clazz.simpleName.asString())
            if (problemNumber == null) {
                logger.error(
                    "The problem solution class does not match the 'SolutionXXXX' pattern, with XXXX being " +
                            "a number (leading zeroes are required if XXXX < 1000).",
                    clazz
                )
                return emptyList()
            }
        }

        return emptyList()
    }

    companion object {

        /**
         * The regex all problem solution classes should match.
         */
        private val solutionNumberRegex = Regex("""Solution(\d{4})""")

        /**
         * Extracts the problem number from the class name. All the problem classes are required to have a name matching
         * the [solutionNumberRegex] regex pattern. If they don't, they are not allowed to be annotated with the
         * [me.djsushi.kotlinleet.processor.api.Solution] annotation.
         */
        fun extractNumberFromClassName(className: String) =
            solutionNumberRegex.find(className)?.groups?.get(1)?.value?.toInt()
    }

    class Provider : SymbolProcessorProvider {

        override fun create(environment: SymbolProcessorEnvironment): SymbolProcessor = Processor(environment.logger)
    }
}