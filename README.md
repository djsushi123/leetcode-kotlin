# LeetCode Solutions in Kotlin

## Motivation

The reason why I started this project is that I wanted to build my first Kotlin annotation processor using KSP.

## Goal

My goal with this project is to build a functional annotation processor in Kotlin that will automatically build and test
all my solutions for the problems. Maybe in the future, I will even build a scraper of the LeetCode website and
autogenerate a nice page for each problem with the problem's description and my solution. Well, we will see where this
goes...