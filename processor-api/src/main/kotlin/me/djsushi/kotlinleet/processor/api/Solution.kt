package me.djsushi.kotlinleet.processor.api

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class Solution(val entryName: String)
